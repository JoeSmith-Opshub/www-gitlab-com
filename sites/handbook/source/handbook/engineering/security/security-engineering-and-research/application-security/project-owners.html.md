---
layout: handbook-page-toc
title: "Application Security Project owners"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Scope

This page defines and divides the ownership of the appsec team high level responsibilities.

## Ownership table

| Project/Category | Owner/DRI | 
|---|---|
| Metrics  | [James Ritchey](https://gitlab.com/jritchey) | 
| HackerOne/Bug Bounty  | [James Ritchey](https://gitlab.com/jritchey) | 
| Stable Counterpart vision  | [James Ritchey](https://gitlab.com/jritchey) | 
| 3rd party testing (pentests)  | [James Ritchey](https://gitlab.com/jritchey) | 
| Container Scanner Evaluation  | [Ethan Strike](https://gitlab.com/estrike) | 
| Appsec review process/self-service threat modeling | [Ethan Strike](https://gitlab.com/estrike) |
| Career path development | [Ethan Strike](https://gitlab.com/estrike) |
| Hiring  | [James Ritchey](https://gitlab.com/jritchey) and [Ethan Strike](https://gitlab.com/estrike) | 
| Quarterly Vulnerability Initiatives  | [James Ritchey](https://gitlab.com/jritchey) and [Ethan Strike](https://gitlab.com/estrike) | 
