---
layout: markdown_page
title: "Surveys"
---

## Overview
This page focuses on surveys that we or a 3rd party sends out and is a tactic used within marketing campaigns, including account centric campaigns. If you are in Field Marketing, please see [this page](/handbook/marketing/field-marketing/field-marketing-epics/#content-syndication) for Field Marketing epic details.

### Process in GitLab to organize epic & issues

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner creates the main tactic issue
1. Project owner creates the epic to house all related issues (code below)
1. Project owner creates the relevant issues required (shortcut links in epic code below)
1. Project owner associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of a survey changes, Project owner (FM) is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

```
<!-- Name this epic using the following format, then delete this line: Survey - [Vendor] - [3-letter Month] [Date], [Year] --->

## [Main Issue >>]()

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner:** 
* **Coordinator:** 
* **Type:** Survey
* **Sales Segment (choose primary):** `Large, Mid-Market, or SMB`
* **Sales Region (choose one):** `AMER, EMEA, APAC`
* **Sales Territory (optional, if specific):** 
* **Goal:** `Please be specific on the metric this is meant to impact.`
* **Campaign Tag:**  
* **Budget:** 
* **Launch Date:**  [MM-DD-YYYY] (this is the date the survey begins through the vendor)
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[What is the anticipated user experience? Project owner to provide a description of the journey as if they were the end user - from communications received (both from GitLab and/or from vendor), to what the user provides in survey, what happens after they submit, what do they receive from us after we upload the leads, and beyond... what is the end-to-end journey for the user?]

## Additional description and notes about the tactic
[Project owner add whatever additional notes are relevant here]

## Issue Creation

* [ ] [Program Tracking](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-program-tracking)
* [ ] [List clean and upload issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) 
* [ ] [Follow up email issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email-followup) 
* [ ] [Add to nurture issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-add-nurture) 

**Optional: create the optional issues only we have rights to survey results and there is a multi-channel plan for how the survey will be promoted*

/label~"mktg-status::wip" ~"Survey"
```


### SimplyDirect <> Marketo Integration
SimplyDirect and Marketo are integrated, so that SimplyDirect can send leads to Marketo immediately without a list upload. You must follow [these directions](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up) exactly, each time you set up a new survey program, or else the integration will not work. 
